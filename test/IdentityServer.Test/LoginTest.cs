//using ClienteApi2;
using IdentityModel;
using IdentityModel.Client;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using static IdentityModel.OidcConstants;

namespace IdentityServer.Test
{
    // OneTimeSetup 
    /** https://xunit.net/docs/shared-context */
    public class LoginTest
    {
        private readonly HttpClient _httpClient;
        private Task<DiscoveryDocumentResponse> disco;
        private readonly TokenClient _tokenClient;
        private Task<IdentityModel.Client.TokenResponse> _tokenResponse2;

        public LoginTest()
        {
            //var webhost = new WebHostBuilder()
            //.UseUrls("http://*:5000")
            //.UseStartup<Startup>();

            //var server = new TestServer(webhost);
            //_httpClient = new HttpClient();//server.CreateClient();

            //disco = _httpClient.GetDiscoveryDocumentAsync("http://localhost:5000");           

        }

        [Fact]
        public async void ShouldNotAllowAnonymousUser()
        {
            var client3 = new HttpClient();
            var result = await client3.GetAsync("http://localhost:5002/identity");
            Assert.Equal(HttpStatusCode.Unauthorized, result.StatusCode);
        }

        [Fact]
        public async void ShouldReturnValuesForAuthenticatedUser()
        {
            var ClientIdentityServer = new HttpClient();//server.CreateClient();

            disco = ClientIdentityServer.GetDiscoveryDocumentAsync("http://localhost:5000/");

            var client3 = new HttpClient();
            _tokenResponse2 = client3.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.Result.TokenEndpoint,
                GrantType = GrantTypes.Password,
                ClientId = "clienteApiDois",
                ClientSecret = "secret",
                Scope = "IdentityServerControl",

                UserName = "celio",
                Password = "1234".ToSha256()
            });
            _httpClient.SetBearerToken(_tokenResponse2.Result.AccessToken);

            var result = await client3.GetAsync("http://localhost:5002/identity");
            Assert.Equal(result.StatusCode, HttpStatusCode.OK);
        }
    }
}
