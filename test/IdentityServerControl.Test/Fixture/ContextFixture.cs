﻿
using IdentityServerControl.Test.Theory;
using IdentityServerControl.Models;
using IdentityServerControl.Options;
using IdentityServerControl.Repositories;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace IdentityServerControl.Test.Fixture
{
    public class ContextFixture : IDisposable
    {
        public UserRepository testUserRepository;

        public ContextFixture()
        {
            ConfigurationOptions app = new ConfigurationOptions() { MongoConnection = "mongodb://localhost:27017", MongoDatabaseName="IdentityServer" };
            var mock = new Mock<IOptions<ConfigurationOptions>>();
            // We need to set the Value of IOptions to be the SampleOptions Class
            mock.Setup(ap => ap.Value).Returns(app);

            testUserRepository = new UserRepository(mock.Object);

            // mock data created by https://barisates.github.io/pretend
            testUserRepository.Add(new UserTheoryData().GetApplicationUser());
        }

        // https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1063?view=vs-2019
        #region ImplementIDisposableCorrectly
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // NOTE: Leave out the finalizer altogether if this class doesn't
        // own unmanaged resources, but leave the other methods
        // exactly as they are.
        ~ContextFixture()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (testUserRepository != null)
                {
                    testUserRepository = null;
                }
            }
        }
        #endregion
    }
}
