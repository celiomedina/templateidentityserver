﻿using IdentityServerControl.Test.Theory;
using IdentityServerControl.Controllers;
using IdentityServerControl.Models;
using IdentityServerControl.Options;
using IdentityServerControl.Repositories;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using IdentityServerControl.Infrastructure.Repositories;

namespace IdentityServerControl.Test.Fixture
{
    public class ControllerFixture : IDisposable
    {
        private UserRepository userRepository { get; set; }
        private IdentityUserClaimRepository identityUserRepository { get; set; }

        public UserController userController { get; private set; }

        public ControllerFixture()
        {
            #region Create mock/memory database

            ConfigurationOptions app = new ConfigurationOptions() { MongoConnection = "mongodb://localhost:27017", MongoDatabaseName = "identity4db" };
            var mock = new Mock<IOptions<ConfigurationOptions>>();
            // We need to set the Value of IOptions to be the SampleOptions Class
            mock.Setup(ap => ap.Value).Returns(app);

            userRepository = new UserRepository(mock.Object);
            identityUserRepository = new IdentityUserClaimRepository(mock.Object);


            // mock data created by https://barisates.github.io/pretend
            userRepository.Add(new UserTheoryData().GetApplicationUser());

            #endregion

            // Create Controller
            userController = new UserController(userRepository, identityUserRepository);

        }

        #region ImplementIDisposableCorrectly
        /** https://docs.microsoft.com/en-us/visualstudio/code-quality/ca1063?view=vs-2019 */
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // NOTE: Leave out the finalizer altogether if this class doesn't
        // own unmanaged resources, but leave the other methods
        // exactly as they are.
        ~ControllerFixture()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                userRepository = null;

                userController = null;
            }
        }
        #endregion
    }
}
