﻿using IdentityServerControl.Controllers.Models;
using IdentityServerControl.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using Xunit;

namespace IdentityServerControl.Test.Theory
{
    public class UserTheoryData: TheoryData<ApplicationUser>
    {
        public UserTheoryData()
        {
            /**
             * Each item you add to the TheoryData collection will try to pass your unit test's one by one.
             */

            // mock data created by https://barisates.github.io/pretend
            Add(new ApplicationUser("teste")
            {
                Email = new Range(0, 6).ToString(),
                SubjectId = "1",
                Username = new Range(0, 6).ToString(),
                Id = Guid.NewGuid().ToString(),
                Password = new Range(0, 6).ToString(),
                IsActive = true
            });
        }

        public UserRegisterModel GetUserRegisterModel()
        {
            return new UserRegisterModel()
            {  
                SubjectId = new Range(0, 6).ToString(),
                Username = new Range(0,6).ToString(),
                Password = "d87btl",
                IsActive = true,
                Claims = new List<ClaimModel>()
                {
                    new ClaimModel()
                    {
                        Type = "Test.Role",
                        Value = "Test.Value"
                    }
                }                
            };
        }

        public ApplicationUser GetApplicationUser()
        {
            return new ApplicationUser(new Range(0, 6).ToString())
            {
                Email = new Range(0, 6).ToString(),
                SubjectId = new Range(0, 6).ToString(),
                Username = new Range(0, 6).ToString(),
                Id = Guid.NewGuid().ToString(),
                Password = "d87btl",
                IsActive = true
            };
        }
    }
}
