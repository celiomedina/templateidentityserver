﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClienteApi2.Models
{
    public class ClaimModel
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
