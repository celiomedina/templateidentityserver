﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Flurl.Util;
using IdentityModel.Client;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static IdentityModel.OidcConstants;

namespace ClienteApi2.Controllers
{
    public class HotelController : Controller
    {

        /// <summary>
        /// Chamada de Login por Api
        /// </summary>
        /// <param name="user"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        [HttpGet("Login")]

        public async Task<string> LoginAsync(string user, string pwd)
        {
            StringBuilder result = new StringBuilder();
            var client2 = new HttpClient();
            var disco2 = await client2.GetDiscoveryDocumentAsync("http://localhost:5000");
            if (disco2.IsError)
            {
                Console.WriteLine(disco2.Error);
                return "";
            }

            string usuario = user;
            string senha = pwd;

            var tokenResponse2 = await client2.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco2.TokenEndpoint,
                GrantType = GrantTypes.Password,
                ClientId = "clienteApiDois",
                ClientSecret = "secret",
                Scope = "SistemaReservas",

                UserName = usuario,
                Password = senha
            });

            if (!tokenResponse2.IsError)
            {
                result.Append($"JWT{Environment.NewLine}{Environment.NewLine}");
                result.Append(tokenResponse2.AccessToken);

                result.Append($"{Environment.NewLine}{Environment.NewLine}JSON{Environment.NewLine}{Environment.NewLine}");
                result.Append(tokenResponse2.Json);
            }
            else
            {
                result.Append(tokenResponse2.Error);
                //return;
            }

            return result.ToString();
        }


        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Todo
        ///     {
        ///       "NomeHotel": "1",
        ///       "Acomodacao": "username",
        ///       "Periodo": "password",
        ///       "NomeColaborador": "provider1",
        ///       "Documento": "1"
        ///     }
        ///
        /// </remarks>
        /// <param name="model"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("Reserva")]
        public async Task<ObjectResult> Reserva(ReservaModel model, string token)
        {
            string url = $"http://localhost:5005/Hotel/Reserva";

            var json = JsonConvert.SerializeObject(model);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var client3 = new HttpClient();
            if (token != null) client3.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var apiResponse = client3.PostAsync(url, stringContent).Result;

            if (apiResponse.IsSuccessStatusCode)
                return Ok(apiResponse.ReasonPhrase);
            else
                return StatusCode((int)apiResponse.StatusCode, apiResponse.ReasonPhrase);
        }
    }

    public class ReservaModel
    {
        public string NomeHotel { get; set; }
        public TipoAcomodacao Acomodacao { get; set; }
        public string Periodo { get; set; }
        public string NomeColaborador { get; set; }
        public string Documento { get; set; }
    }

    public enum TipoAcomodacao
    {
        CasalStandard,
        SolteiroStandard,
        SuiteMaster
    }
}