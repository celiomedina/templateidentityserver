﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClienteApi2.Controllers.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Flurl;
using Flurl.Http;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using IdentityModel.Client;
using static IdentityModel.OidcConstants;
using System.Text;
using System.IO;
using System.Net;
using Refit;

namespace ClienteApi2.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("[controller]")]
    public class UserController : Controller
    {
        public UserController()
        {

        }

        /// <summary>
        /// Chamada de Login por Api
        /// </summary>
        /// <param name="user"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        [HttpGet("Login")]

        public async Task<string> LoginAsync(string user, string pwd)
        {
            StringBuilder result = new StringBuilder();
            var client2 = new HttpClient();
            var disco2 = await client2.GetDiscoveryDocumentAsync("http://localhost:5000");
            if (disco2.IsError)
            {
                Console.WriteLine(disco2.Error);
                return "";
            }

            string usuario = user;
            string senha = pwd;

            var tokenResponse2 = await client2.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco2.TokenEndpoint,
                GrantType = GrantTypes.Password,
                ClientId = "clienteApiDois",
                ClientSecret = "secret",
                Scope = "IdentityServerControl",

                UserName = usuario,
                Password = senha
            });

            if (!tokenResponse2.IsError)
            {
                result.Append($"JWT{Environment.NewLine}{Environment.NewLine}");
                result.Append(tokenResponse2.AccessToken);

                result.Append($"{Environment.NewLine}{Environment.NewLine}JSON{Environment.NewLine}{Environment.NewLine}");
                result.Append(tokenResponse2.Json);
            }
            else
            {
                result.Append(tokenResponse2.Error);
                //return;
            }

            return result.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Todo
        ///     {
        ///       "subjectId": "1",
        ///       "username": "username",
        ///       "password": "password",
        ///       "isActive": true
        ///     }
        ///
        /// </remarks>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost("Create")]        
        public async Task<bool> Create([FromBody]UserRegisterModel model, string token, string _returnUrl = null)
        {
            string url = $"http://localhost:5002/User/Create";//?SubjectId={model.SubjectId}&Username={model.Username}&Password={model.Password}&ProviderName={model.ProviderName}&ProviderSubjectId={model.ProviderSubjectId}&IsActive={model.IsActive}&Claims=Marketing&token={token}&_returnUrl={_returnUrl}";
            
            var json = JsonConvert.SerializeObject(model);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var client3 = new HttpClient();
            client3.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var apiResponse = client3.PostAsync(url, stringContent).Result;

            

            return apiResponse.StatusCode == HttpStatusCode.OK;
        }
    }
}