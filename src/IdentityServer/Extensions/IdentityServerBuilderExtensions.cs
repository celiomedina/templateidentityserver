﻿using IdentityServer.Interfaces;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using Microsoft.Extensions.DependencyInjection;
using IdentityServer.Store;
using IdentityServer4.Test;
using IdentityServer.Services;
using IdentityServer.Repositories;
using IdentityServer.Infrastructure;
using MongoDB.Driver;
using Microsoft.Extensions.Options;
using IdentityServer.Options;
using System.Runtime.CompilerServices;
using IdentityServer.Models;
using IdentityServer.Infrastructure.Repositories;
using IdentityServer.Infrastructure.Repositories.Interfaces;

namespace IdentityServer.Extension
{
    public static class IdentityServerBuilderExtensions
    {
        public static IIdentityServerBuilder AddRepositories(this IIdentityServerBuilder builder)
        {
            builder.Services.AddTransient<IUserRepository, UserRepository>();
            builder.Services.AddTransient<IClientRepository, ClientRepository>();
            builder.Services.AddTransient<IPersistedGrantRepository, PersistedGrantRepository>();
            builder.Services.AddTransient<IResourceRepository, ResourceRepository>();
            builder.Services.AddTransient<IApiResourceRepository, ApiResourceRepository>();
            builder.Services.AddTransient<IIdentityResourceRepository, IdentityResourceRepository>();
            builder.Services.AddTransient<IUserClaimRepository, UserClaimRepository>();

            return builder;
        }

        public static IIdentityServerBuilder AddServices(this IIdentityServerBuilder builder)
        {
            builder.AddResourceOwnerValidator<ResourceOwnerPasswordValidatorService>();
            builder.Services.AddTransient<ILoginService<ApplicationUser>, LoginService>();
            builder.Services.AddTransient<ICorsPolicyService, InMemoryCorsPolicyService>();
            builder.Services.AddSingleton<IPersistedGrantStore, PersistedGrantService>();
            builder.Services.AddTransient<IClientStore, ClientService>();
            builder.Services.AddTransient<IResourceStore, ResourceService>();

            return builder;
        }

    }
}
