﻿using IdentityServer.Infrastructure;
using IdentityServer.Interfaces;
using IdentityServer.Options;
using IdentityServer.Services;
using IdentityServer4.Models;
using IdentityServer4.Test;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IdentityServer.Repositories
{
    public class PersistedGrantRepository : RepositoryBase<PersistedGrant>, IPersistedGrantRepository
    {
        //public PersistedGrantRepository(IMongoDatabase mongoDatabase) : base(mongoDatabase)
        //{

        //}

        public PersistedGrantRepository(IOptions<ConfigurationOptions> optionsAccessor) : base(optionsAccessor)
        {

        }
    }
}
