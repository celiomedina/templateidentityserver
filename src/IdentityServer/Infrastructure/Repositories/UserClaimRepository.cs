﻿using IdentityServer.Infrastructure.Repositories.Interfaces;
using IdentityServer.Models;
using IdentityServer.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Infrastructure.Repositories
{
    public class UserClaimRepository : RepositoryBase<UserClaim>, IUserClaimRepository
    {
        public UserClaimRepository(IOptions<ConfigurationOptions> optionsAccessor) : base(optionsAccessor)
        {
        }
    }
}
