﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IdentityServer.Interfaces
{
    /// <summary>
    /// Basic interface with a few methods for adding, deleting, and querying data.
    /// </summary>
    public interface IRepositoryBase<T> where T : class
    {
        IQueryable<T> All();
        IQueryable<T> Where(Expression<Func<T, bool>> expression);
        T Single(Expression<Func<T, bool>> expression);
        void Delete(Expression<Func<T, bool>> expression);
        void Add(T item);
        void Add(IEnumerable<T> items);
        bool CollectionExists();        
    }
}
