﻿using IdentityServer.Infrastructure;
using IdentityServer.Interfaces;
using IdentityServer.Options;
using IdentityServer4.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace IdentityServer.Repositories
{
    public class IdentityResourceRepository : RepositoryBase<IdentityResource>, IIdentityResourceRepository
    {
        //public IdentityResourceRepository(IMongoDatabase mongoDatabase) : base(mongoDatabase)
        //{

        //}

        public IdentityResourceRepository(IOptions<ConfigurationOptions> optionsAccessor) : base(optionsAccessor)
        {

        }
    }

}
