﻿using IdentityServer.Infrastructure;
using IdentityServer.Interfaces;
using IdentityServer.Models;
using IdentityServer.Options;
using IdentityServer.Services;
using IdentityServer4.Models;
using IdentityServer4.Test;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IdentityServer.Repositories
{
    public class UserRepository : RepositoryBase<ApplicationUser>, IUserRepository
    {
        //public UserRepository(IMongoDatabase mongoDatabase) : base(mongoDatabase)
        //{
        //    _mongoDatabase = mongoDatabase;
        //}

        public UserRepository(IOptions<ConfigurationOptions> optionsAccessor) : base(optionsAccessor)
        {
        }

        public ApplicationUser FindBySubjectId(string subjectId)
        {
            return this.All().Where<ApplicationUser>(c => c.SubjectId.Equals(subjectId)).FirstOrDefault();
        }
    }
}
