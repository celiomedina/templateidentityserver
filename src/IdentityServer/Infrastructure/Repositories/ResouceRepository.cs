﻿using IdentityServer.Infrastructure;
using IdentityServer.Interfaces;
using IdentityServer.Options;
using IdentityServer4.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace IdentityServer.Repositories
{
    public class ResourceRepository : RepositoryBase<Resource>, IResourceRepository
    {
        //public ResourceRepository(IMongoDatabase mongoDatabase) : base(mongoDatabase)
        //{

        //}

        public ResourceRepository(IOptions<ConfigurationOptions> optionsAccessor) : base(optionsAccessor)
        {

        }
    }

}
