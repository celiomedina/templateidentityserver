﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityModel;
using IdentityServer.Models;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using Microsoft.AspNetCore.Identity;
using MongoDB.Bson;
using System.Collections.Generic;
using System.Security.Claims;

namespace IdentityServer.Configuration
{
    public class IdentityServerDBSeed
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
                new IdentityResource("roles", new[] { "role" })
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {               
                new ApiResource("IdentityServerControl", "Api de Controle do IdentityServer"),
                new ApiResource("SistemaReservas", "Api Sistema de Reservas")
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {                
                new Client
                {
                    ClientId = "clienteApiDois",
                    AllowedGrantTypes  =  GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                    AllowOfflineAccess = true,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = {
                        "IdentityServerControl",
                        "SistemaReservas",
                        IdentityServerConstants.StandardScopes.OpenId,
                       IdentityServerConstants.StandardScopes.Profile,
                       "roles"
                    },
                    AccessTokenType = AccessTokenType.Jwt
                }               
            };
        }

        public static List<ApplicationUser> GetUsers()
        {
            return new List<ApplicationUser>
            {
                new ApplicationUser
                {
                    SubjectId = "1",
                    Username = "celio",
                    Password = "1234".Sha256()                 
                },
                new ApplicationUser
                {
                    SubjectId = "2",
                    Username = "bob",
                    Password = "1234".Sha256()
                }
            };
        }

        public static List<UserClaim> GetIdentityClaims(string idUser)
        {
            return new List<UserClaim>
            {
                new UserClaim
                {
                    UserId = idUser,
                    ClaimType = "role", 
                    ClaimValue = "Administrator"
                }
            };
        }
    }
}