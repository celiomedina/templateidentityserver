﻿using System.Threading.Tasks;
using IdentityServer.Interfaces;
using IdentityServer4.Models;

namespace IdentityServer.Store
{
    public class ClientService : IdentityServer4.Stores.IClientStore
    {
        protected IClientRepository _dbRepository;

        public ClientService(IClientRepository repository)
        {
            _dbRepository = repository;
        }

        public Task<Client> FindClientByIdAsync(string clientId)
        {
            var client = _dbRepository.Single(c => c.ClientId == clientId);

            return Task.FromResult(client);
        }
    }
}
