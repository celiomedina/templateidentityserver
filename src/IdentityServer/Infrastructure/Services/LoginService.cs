﻿using IdentityServer.Interfaces;
using IdentityServer.Models;
using IdentityServer4.Test;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Services
{
    public class LoginService : ILoginService<ApplicationUser>
    {
        private IUserRepository _repository;

        public LoginService(IUserRepository repository)
        {
            _repository = repository;
        }

        public Task<ApplicationUser> FindByUsername(string user)
        {
            return Task.FromResult(_repository.Where(c => c.Username.Equals(user)).First());
        }

        //Aqui você implementa como quer a validação do usuário e senha, criptografia, etc.
        public async Task<bool> ValidateCredentials(ApplicationUser user, string password)
        {
            return _repository.Where(c => c.Username.Equals(user.Username) && c.Password.Equals(password)).Any(); 
        }

        public async Task<bool> ValidateCredentials(string userName, string password)
        {
            return _repository.Where(c => c.Username.Equals(userName) && c.Password.Equals(password)).Any();
        }
    }
}
