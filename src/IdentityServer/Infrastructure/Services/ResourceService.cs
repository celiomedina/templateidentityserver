﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer.Interfaces;
using IdentityServer4.Models;
using IdentityServer4.Stores;

namespace IdentityServer.Services
{
    public class ResourceService : IResourceStore
    {
        protected IResourceRepository _resourceRepository;
        protected IApiResourceRepository _apiResourceRepository;
        protected IIdentityResourceRepository _IdentityResourceRepository;

        public ResourceService(IResourceRepository resourceRepository, IApiResourceRepository apiResouceRepository, IIdentityResourceRepository IdentityResourceRepository)
        {
            _resourceRepository = resourceRepository;
            _apiResourceRepository = apiResouceRepository;
            _IdentityResourceRepository = IdentityResourceRepository;
        }

        private IEnumerable<ApiResource> GetAllApiResources()
        {
            return _apiResourceRepository.All().AsEnumerable();
        }

        private IEnumerable<IdentityResource> GetAllIdentityResources()
        {
            return _IdentityResourceRepository.All();
        }

        public Task<ApiResource> FindApiResourceAsync(string name)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));

            return Task.FromResult(_apiResourceRepository.Single(a => a.Name == name));
        }

        public Task<IEnumerable<ApiResource>> FindApiResourcesByScopeAsync(IEnumerable<string> scopeNames)
        {
            var list = _apiResourceRepository.Where(a => a.Scopes.Any(s => scopeNames.Contains(s.Name)));

            return Task.FromResult(list.AsEnumerable());
        }

        public Task<IEnumerable<IdentityResource>> FindIdentityResourcesByScopeAsync(IEnumerable<string> scopeNames)
        {
            var list = _IdentityResourceRepository.Where(e => scopeNames.Contains(e.Name));

            return Task.FromResult(list.AsEnumerable());
        }

        public Resources GetAllResources()
        {
            var result = new Resources(GetAllIdentityResources(), GetAllApiResources());
            return result;
        }

        private Func<IdentityResource, bool> BuildPredicate(Func<IdentityResource, bool> predicate)
        {
            return predicate;
        }

        public Task<Resources> GetAllResourcesAsync()
        {
            var result = new Resources(GetAllIdentityResources(), GetAllApiResources());
            return Task.FromResult(result);
        }
    }
}
