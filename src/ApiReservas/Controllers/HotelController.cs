﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApiReservas.Controllers
{
    [Route("Hotel")]
    [Authorize]
    public class HotelController : Controller
    {

        public HotelController()
        { }
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Solicitar Reserva
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("Reserva")]
        public IActionResult Reserva(ReservaModel model)
        {
            return Ok("Reserva solicitada com sucesso!");
        }
    }

    public class ReservaModel
    {
        public string NomeHotel { get; set; }
        public TipoAcomodacao Acomodacao { get; set;}
        public string Periodo { get; set; }
        public string NomeColaborador { get; set; }
        public string Documento { get; set; }
    }

    public enum TipoAcomodacao {
        CasalStandard, 
        SolteiroStandard, 
        SuiteMaster
    }
}