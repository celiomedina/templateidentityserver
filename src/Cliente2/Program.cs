﻿using IdentityModel;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Cliente2
{
    class Program
    {
        static async Task Main()
        {
            #region Autenticando para API2

            Console.WriteLine("Início da autenticação com a Api2 com usuário e senha");

            // aqui nós pegamos as configurações do nosso servidor através da URL do mesmo.
            // precisamos pegar o EndPoint do token para podermos fazer o login.
            var client2 = new HttpClient();
            var disco2 = await client2.GetDiscoveryDocumentAsync("http://localhost:5000");
            if (disco2.IsError)
            {
                Console.WriteLine(disco2.Error);
                return;
            }

            
            #region Autenticação com usuário e senha
            //Requisição com usuário e senha
            var erro = true;
            while (erro)
            {
                Console.WriteLine("Iniciando a autenticação na Api2");
                Console.WriteLine("Digite seu usuário: ");
                string usuario = Console.ReadLine();
                Console.WriteLine("Digite senha: ");
                string senha = Console.ReadLine();
                var tokenResponse2 = await client2.RequestPasswordTokenAsync(new PasswordTokenRequest
                {
                    Address = disco2.TokenEndpoint,
                    GrantType = "password",

                    ClientId = "clientDois",
                    ClientSecret = "secretConsoleDois",
                    Scope = "api2",

                    UserName = usuario,
                    Password = senha
                });
                // caso não tenha erro, escrevemos o token de acesso bem como as configurações e
                // permissões que o usuário logado tem
                if (!tokenResponse2.IsError)
                {
                    var client3 = new HttpClient();
                    client3.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenResponse2.AccessToken);
                    var apiResponse = await client3.GetAsync("http://localhost:5002/identity");

                    if (!apiResponse.IsSuccessStatusCode)
                    {
                        Console.WriteLine(apiResponse.StatusCode);
                    }
                    else
                    {
                        var content = await apiResponse.Content.ReadAsStringAsync();
                        Console.WriteLine(content);
                        Console.WriteLine("********************");
                        Console.WriteLine("Json");
                        Console.WriteLine(tokenResponse2.Json);

                        Console.WriteLine();
                        //Token de acesso
                        Console.WriteLine("Token de acesso!");
                        Console.WriteLine(tokenResponse2.AccessToken);
                        
                        
                    }
                    erro = false;
                    Console.ReadLine();
                }// Se tiver um erro, nós escrevemos no console
                else
                {
                    Console.WriteLine(tokenResponse2.Error);
                    //return;
                }
            }
            #endregion
            #endregion
            Console.WriteLine("Aperte qualquer tecla para finalizar!");
            Console.ReadLine();

            return;
        }        


    }
}
