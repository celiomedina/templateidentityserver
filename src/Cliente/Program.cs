﻿using IdentityModel;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Cliente
{
    class Program
    {
        static async Task Main()
        {

            #region Autenticando para a API1
            Console.WriteLine("Este exemplo está sendo configurando para autenticar com usuário e senha");
            Console.WriteLine("Início da autenticação com a Api 1 sem passar o usuário e senha");

            // aqui nós pegamos as configurações do nosso servidor através da URL do mesmo.
            // precisamos pegar o EndPoint do token para podermos fazer o login.
            var client = new HttpClient();
            var disco = await client.GetDiscoveryDocumentAsync("http://localhost:5000");
            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                return;
            }

            // nesta parte, temos um exemplo de requisição com o tipo "password" 
            // esta é a forma mais comum
            var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                //variável disco com o metadata(documento de descoberta), pela disco.TokenEndpoint o tem o endereço
                //de solicitação do Token
                Address = disco.TokenEndpoint,

                ClientId = "client",
                ClientSecret = "secretConsole",
                Scope = "api1"
            });

            // Se tiver um erro, nós escrevemos no console
            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                //return;
            }
            // caso não tenha erro, escrevemos o token de acesso bem como as configurações e
            // permissões que o usuário logado tem
            Console.WriteLine("Token de resposta");
            Console.WriteLine(tokenResponse.Json);

            // agora chamamos a api segura passando um token de acesso fornecido pelo servidor quando logamos
            // dessa forma, precisamos passa-lo no header da requisição
            // que é exatamente o que fazemos nessa parte
            var apiClient = new HttpClient();
            apiClient.SetBearerToken(tokenResponse.AccessToken);

            var response = await apiClient.GetAsync("http://localhost:5001/identity");
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine(content);
            }

            #endregion

            //var responseCallIdentity = await apiClient.GetAsync("http://localhost:5001/identity");
            //if (!responseCallIdentity.IsSuccessStatusCode)
            //{
            //    Console.WriteLine(responseCallIdentity.StatusCode);
            //}
            //else
            //{
            //    var content = await responseCallIdentity.Content.ReadAsStringAsync();
            //    Console.WriteLine(content);
            //}

            return;
        }        


    }
}
