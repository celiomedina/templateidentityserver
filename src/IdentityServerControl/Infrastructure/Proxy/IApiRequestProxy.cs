﻿using IdentityServerControl.Controllers.Models;
using System.Threading.Tasks;

namespace IdentityServerControl.Infrastructure.Proxy
{
    public interface IApiRequestProxy
    {
        Task<bool> Add(UserRegisterModel user);
    }
}
