﻿using System.Threading.Tasks;
using Flurl;
using Microsoft.Extensions.Options;
using Flurl.Http;
using IdentityServerControl.Controllers.Models;
using IdentityServerControl.Options;

namespace IdentityServerControl.Infrastructure.Proxy
{
    public class ApiRequestProxy : IApiRequestProxy
    {
        private readonly ConfigurationOptions configuracao;
        public ApiRequestProxy(IOptions<ConfigurationOptions> options)
        {
            configuracao = options.Value;
        }
        public Task<bool> Add(UserRegisterModel user)
        {
            return Task.FromResult(configuracao.Url
                 .AppendPathSegment(configuracao.EndpointAddUser)
                 .PostJsonAsync(user).Result.StatusCode == System.Net.HttpStatusCode.OK);
        }
    }
}
