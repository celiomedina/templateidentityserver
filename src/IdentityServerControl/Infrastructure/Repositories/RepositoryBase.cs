﻿using MongoDB.Bson;
using MongoDB.Driver;
using IdentityServerControl.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using IdentityServerControl.Options;
using IdentityServerControl.Interfaces;

namespace IdentityServerControl.Infrastructure
{

    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        public readonly IMongoDatabase _context = null;
        protected static IMongoClient _client;


        public RepositoryBase(IOptions<ConfigurationOptions> optionsAccessor)
        {
            var configurationOptions = optionsAccessor.Value;

            _client = new MongoClient(configurationOptions.MongoConnection);
            _context = _client.GetDatabase(configurationOptions.MongoDatabaseName);
        }

        public IQueryable<T> All()
        {
            return _context.GetCollection<T>(typeof(T).Name).AsQueryable();
        }
        public IQueryable<T> Where(System.Linq.Expressions.Expression<Func<T, bool>> expression)
        {
            return All().Where(expression);
        }
        public void Delete(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            var result = _context.GetCollection<T>(typeof(T).Name).DeleteMany(predicate);

        }
        public T Single(System.Linq.Expressions.Expression<Func<T, bool>> expression)
        {
            return All().Where(expression).SingleOrDefault();
        }
        public bool CollectionExists()
        {
            var collection = _context.GetCollection<T>(typeof(T).Name);
            var filter = new BsonDocument();
            var totalCount = collection.CountDocuments(filter);
            return (totalCount > 0) ? true : false;
        }
        public void Add(T item)
        {
            _context.GetCollection<T>(typeof(T).Name).InsertOne(item);
        }
        public void Add(IEnumerable<T> items)
        {
            _context.GetCollection<T>(typeof(T).Name).InsertMany(items);
        }
    }
}
