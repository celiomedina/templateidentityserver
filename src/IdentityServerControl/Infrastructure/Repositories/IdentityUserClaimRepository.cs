﻿using IdentityServerControl.Infrastructure.Repositories.Interfaces;
using IdentityServerControl.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServerControl.Infrastructure.Repositories
{
    public class IdentityUserClaimRepository : RepositoryBase<IdentityUserClaim<string>>, IIdentityUserClaimRepository
    {
        public IdentityUserClaimRepository(IOptions<ConfigurationOptions> optionsAccessor) : base(optionsAccessor)
        {
        }
    }
}
