﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Flurl.Util;
using IdentityServer4.Models;
using IdentityServerControl.Controllers.Models;
using IdentityServerControl.Infrastructure.Proxy;
using IdentityServerControl.Infrastructure.Repositories.Interfaces;
using IdentityServerControl.Interfaces;
using IdentityServerControl.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Refit;

namespace IdentityServerControl.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("[controller]")]
    public class UserController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly IIdentityUserClaimRepository _identityUserRepository;
        public UserController(IUserRepository userRepository, IIdentityUserClaimRepository identityUserRepository)
        {
            _userRepository = userRepository;
            _identityUserRepository = identityUserRepository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Todo
        ///     {
        ///       "subjectId": "1",
        ///       "username": "username",
        ///       "password": "password",
        ///       "isActive": true,
        ///       "claims": [
        ///         {
        ///             "type": "SIGSA",
        ///             "value": "USUARIO.EXCLUIR"
        ///         }
        ///       ]
        ///     }
        ///
        /// </remarks>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost("Create")]
        [Headers("Authorization: Bearer")]
        public async Task<ActionResult> Create([FromBody]UserRegisterModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid && model != null)
            {
                var user = new ApplicationUser(model.Username)
                {
                    Username = model.Username,                    
                    IsActive = model.IsActive,
                    Password = model.Password.Sha256(),
                    SubjectId = model.SubjectId                    
                };
                _userRepository.Add(user);
                var userSaved = _userRepository.Where(c => c.Username == user.Username).FirstOrDefault();
                var userClaims = model.Claims.Select(
                    c => new IdentityUserClaim<string>() {
                        UserId = userSaved.Id,
                        ClaimType = c.Type,
                        ClaimValue = c.Value
                    });

                _identityUserRepository.Add(userClaims);

                return Ok();
            }

            return BadRequest();
        }
    }
}